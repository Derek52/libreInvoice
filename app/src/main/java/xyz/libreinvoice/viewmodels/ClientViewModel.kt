package xyz.libreinvoice.viewmodels

import androidx.lifecycle.ViewModel
import xyz.libreinvoice.database.Client

class ClientViewModel : ViewModel() {
    var selectedClient = Client("Empty", "Empty", "Empty")
    var clients : List<Client> = List(1) { selectedClient }
}