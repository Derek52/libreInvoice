package xyz.libreinvoice.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_add_client.*
import xyz.libreinvoice.R
import xyz.libreinvoice.database.AppDatabase
import xyz.libreinvoice.database.Client
import xyz.libreinvoice.database.ClientDao
import xyz.libreinvoice.viewmodels.ClientViewModel
import kotlin.concurrent.thread

/**
 This fragment is used to create new clients, and update existing ones.
 */
class AddClientFragment : Fragment() {

    private val args : AddClientFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_client, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val db = AppDatabase.getDatabase(this.requireContext())
        val clientDao = db.clientDao()

        val client : Client

        if (args.editClient) {
            val model = activity?.run {
                ViewModelProviders.of(this)[ClientViewModel::class.java]
            } ?: throw Exception("Invalid Activity")
            client = model.selectedClient

            clientNameInput.setText(client.name)
            clientEmailInput.setText(client.email)
            clientNumberInput.setText(client.number)
            saveClientButton.text = getString(R.string.edit_client_button)

        } else {
            client = Client()
            saveClientButton.text = getString(R.string.add_client_button)
        }

        saveClientButton.setOnClickListener {
            client.name = clientNameInput.text.toString()
            client.email = clientEmailInput.text.toString()
            client.number = clientNumberInput.text.toString()
            saveClient(client, clientDao)
            if (args.editClient) {
                Toast.makeText(this.requireContext(), getString(R.string.client_updated, client.name), Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this.requireContext(), getString(R.string.client_added, client.name), Toast.LENGTH_LONG).show()
            }
            findNavController().navigate(R.id.action_return_to_client_list)
        }
    }

    private fun saveClient(client : Client, clientDao : ClientDao) {
        thread {
            clientDao.insert(client)
        }
    }
}
