package xyz.libreinvoice.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_show_client.*
import xyz.libreinvoice.R
import xyz.libreinvoice.database.AppDatabase
import xyz.libreinvoice.database.Client
import xyz.libreinvoice.viewmodels.ClientViewModel
import kotlin.concurrent.thread

class ShowClientFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_show_client, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = activity?.run {
            ViewModelProviders.of(this)[ClientViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        clientNameView.text = model.selectedClient.name
        clientEmailView.text = model.selectedClient.email
        clientNumberView.text = model.selectedClient.number
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_show_client_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.show_client_edit -> {
                //val action = ShowClientFragmentDirections.action
                val action = ShowClientFragmentDirections.actionShowClientFragmentToAddClientFragment(true)
                findNavController().navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }
}
