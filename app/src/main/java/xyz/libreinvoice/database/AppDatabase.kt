package xyz.libreinvoice.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Client::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun clientDao() : ClientDao

    companion object {
        lateinit var INSTANCE : AppDatabase
        var databaseBuilt = false
        fun getDatabase(context : Context) : AppDatabase{
            if (databaseBuilt != true) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java,
                    "libreinvoice_database").build()
                databaseBuilt = true
            }
            return INSTANCE
        }
    }
}