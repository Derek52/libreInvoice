package xyz.libreinvoice.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ClientDao {

    @get:Query("SELECT * FROM clients ORDER BY id ASC")
    val clients : List<Client>

    @Query("SELECT * FROM clients WHERE client_name LIKE :name")
    fun findClientByName(name : String) : List<Client>

    @Query("SELECT * FROM clients WHERE id =:clientId")
    fun findClientById(clientId : Long) : Client

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(client : Client)

    @Query("DELETE FROM clients")
    fun deleteAll()

    @Query("DELETE FROM clients WHERE id=:clientId")
    fun deleteClientById(clientId : Long)
}